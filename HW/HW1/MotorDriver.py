'''
@file MotorDriver.py

This file implements a MotorDriver class for controlling an elevator

This file was adapted from an example from the lecture

It implements a virtual driver that controls a virtual motor to move the
elevator. 
'''

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''

    ## Constant representing a stationary (off) motor
    STATIONARY = 0

    ## Constant representing a motor moving upwards
    UP = 1

    ## Constant representing a motor moving downwards
    DOWN = 0
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''

        ## Saves the internal state of the motor, initialized to STATIONARY
        self.state = MotorDriver.STATIONARY
        pass
    
    def Up(self):
        '''
        @brief Moves the motor up
        '''
        self.state = MotorDriver.UP
        print('Motor moving Up')
    
    def Down(self):
        '''
        @brief Moves the motor down
        '''
        self.state = MotorDriver.DOWN
        print('Motor moving Down')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        self.state = MotorDriver.STATIONARY
        print('Motor Stopped')

    def getState(self):
        '''
        @brief Gets the motor state
        @return An integer representing the state of the motor 
        '''
        return self.state

