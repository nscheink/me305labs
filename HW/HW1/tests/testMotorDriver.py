import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

from MotorDriver import MotorDriver

class TestMotorDriver(unittest.TestCase):
    def test_init(self):
        motor = MotorDriver()
        self.assertEqual(motor.getState(), MotorDriver.STATIONARY)

    def test_up(self):
        motor = MotorDriver()
        motor.Up()
        self.assertEqual(motor.getState(), MotorDriver.UP)

    def test_down(self):
        motor = MotorDriver()
        motor.Down()
        self.assertEqual(motor.getState(), MotorDriver.DOWN)

    def test_stop(self):
        motor = MotorDriver()
        motor.Down()
        self.assertEqual(motor.getState(), MotorDriver.DOWN)
        motor.Stop()
        self.assertEqual(motor.getState(), MotorDriver.STATIONARY)

    def test_repeated(self):
        motor = MotorDriver()
        for n in range(100):
            self.assertEqual(motor.getState(), MotorDriver.STATIONARY)
            motor.Down()
            self.assertEqual(motor.getState(), MotorDriver.DOWN)
            motor.Up()
            self.assertEqual(motor.getState(), MotorDriver.UP)
            motor.Stop()




if __name__ == '__main__':
    unittest.main() 
