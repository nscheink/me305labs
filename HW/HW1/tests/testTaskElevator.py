import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

from Button import Button
from MotorDriver import MotorDriver
from ProximitySensor import ProximitySensor
from TaskElevator import TaskElevator
from time import sleep

class TestButton(unittest.TestCase):
    DELAY = 0.00001
    def test_init(self):
        motor = MotorDriver()
        fb = Button('0')
        sb = Button('1')
        fps = ProximitySensor('2')
        sps = ProximitySensor('3')
        elA = TaskElevator(
                self.DELAY,
                motor,
                fb,
                sb,
                fps,
                sps
            )
        self.assertEqual(elA.state, TaskElevator.INIT)
        self.assertEqual(elA.Motor, motor)
        self.assertEqual(elA.FirstButton, fb)
        self.assertEqual(elA.SecondButton, sb)
        self.assertEqual(elA.FirstSensor, fps)
        self.assertEqual(elA.SecondSensor, sps)
        self.assertEqual(elA.runs, 0)
        self.assertEqual(elA.interval, self.DELAY)

    def test_init_state(self):
        elA = TaskElevator(
                self.DELAY,
                MotorDriver(),
                Button('0'),
                Button('1'),
                ProximitySensor('2'),
                ProximitySensor('3')
            )
        elA.FirstButton.push()
        elA.SecondButton.push()
        elA.run_current_state()
        self.assertEqual(elA.FirstButton.getButtonState(), Button.OFF)
        self.assertEqual(elA.SecondButton.getButtonState(), Button.OFF)
        self.assertEqual(elA.state, TaskElevator.MOVING_DOWN)
        self.assertEqual(elA.Motor.getState(), MotorDriver.DOWN)

    def test_stopped_at_first(self):
        elA = TaskElevator(
                self.DELAY,
                MotorDriver(),
                Button('0'),
                Button('1'),
                ProximitySensor('2'),
                ProximitySensor('3')
            )
        elA.run_current_state()
        while(elA.state == TaskElevator.MOVING_DOWN):
            elA.run_current_state()
        self.assertEqual(elA.state, TaskElevator.STOPPED_AT_FIRST)
        self.assertEqual(elA.Motor.getState(), MotorDriver.STATIONARY)

        # Make sure first floor button is useless
        elA.FirstButton.push()
        elA.run_current_state()
        self.assertEqual(elA.FirstButton.getButtonState(), Button.OFF)

    def test_moving_up(self):
        elA = TaskElevator(
                self.DELAY,
                MotorDriver(),
                Button('0'),
                Button('1'),
                ProximitySensor('2'),
                ProximitySensor('3')
            )
        elA.run_current_state()
        while(elA.state == TaskElevator.MOVING_DOWN):
            elA.run_current_state()
        
        elA.SecondButton.push()
        elA.run_current_state()
        self.assertEqual(elA.state, TaskElevator.MOVING_UP)
        self.assertEqual(elA.Motor.getState(), MotorDriver.UP)

    def test_stopped_at_second(self):
        elA = TaskElevator(
                self.DELAY,
                MotorDriver(),
                Button('0'),
                Button('1'),
                ProximitySensor('2'),
                ProximitySensor('3')
            )
        elA.run_current_state()
        while(elA.state == TaskElevator.MOVING_DOWN):
            elA.run_current_state()
        
        elA.SecondButton.push()
        elA.run_current_state()
        while(elA.state == TaskElevator.MOVING_UP):
            elA.run_current_state()

        self.assertEqual(elA.state, TaskElevator.STOPPED_AT_SECOND)
        self.assertEqual(elA.Motor.getState(), MotorDriver.STATIONARY)

        # Make sure second floor button is useless
        elA.SecondButton.push()
        elA.run_current_state()
        self.assertEqual(elA.SecondButton.getButtonState(), Button.OFF)

    def test_moving_down_from_second(self):
        elA = TaskElevator(
                self.DELAY,
                MotorDriver(),
                Button('0'),
                Button('1'),
                ProximitySensor('2'),
                ProximitySensor('3')
            )
        elA.run_current_state()
        while(elA.state == TaskElevator.MOVING_DOWN):
            elA.run_current_state()
        
        elA.SecondButton.push()
        elA.run_current_state()
        while(elA.state == TaskElevator.MOVING_UP):
            elA.run_current_state()
        
        elA.FirstButton.push()
        elA.run_current_state()
        self.assertEqual(elA.state, TaskElevator.MOVING_DOWN)
        self.assertEqual(elA.Motor.getState(), MotorDriver.DOWN)

    def test_button_press_mid_move(self):
        elA = TaskElevator(
                self.DELAY,
                MotorDriver(),
                Button('0'),
                Button('1'),
                ProximitySensor('2'),
                ProximitySensor('3')
            )
        elA.run_current_state()
        elA.SecondButton.push()
        while(elA.state == TaskElevator.MOVING_DOWN):
            elA.run_current_state()
        
        elA.run_current_state()
        elA.FirstButton.push()
        while(elA.state == TaskElevator.MOVING_UP):
            elA.run_current_state()
        
        elA.run_current_state()
        self.assertEqual(elA.state, TaskElevator.MOVING_DOWN)
        self.assertEqual(elA.Motor.getState(), MotorDriver.DOWN)

 
if __name__ == '__main__':
    unittest.main()
