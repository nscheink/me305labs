import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

from Button import Button

class TestButton(unittest.TestCase):
    def test_init(self):
        buttonA = Button('PB7')
        self.assertEqual(buttonA.pin, 'PB7')
        self.assertEqual(buttonA.state, Button.OFF)
        self.assertEqual(buttonA.getButtonState(), Button.OFF)

    def test_push(self):
        button = Button('0')
        button.push()
        self.assertEqual(button.getButtonState(), Button.ON)

    def test_clear(self):
        button = Button('0')
        button.push()
        button.clear()
        self.assertEqual(button.getButtonState(), Button.OFF)

    def test_repeated(self):
        button = Button('0')
        for n in range(100):
            self.assertEqual(button.getButtonState(), Button.OFF)
            button.push()
            self.assertEqual(button.getButtonState(), Button.ON)
            button.clear()

if __name__ == '__main__':
    unittest.main()
