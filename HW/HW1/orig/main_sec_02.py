'''
@file main.py
'''

from FSM_example_sec_02 import Button, MotorDriver, TaskWindshield
        
## Motor Object
Motor = MotorDriver()

## Button Object for "go button"
GoButton = Button('PB6')

## Button Object for "left limit"
LeftLimit = Button('PB7')

## Button Object for "right limit"
RightLimit = Button('PB8')

## Task object
task1 = TaskWindshield(0.1, Motor, GoButton, LeftLimit, RightLimit) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
#    task2.run()
#    task3.run()
