'''
@file FSM_example.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary windshield
wiper.

The user has a button to turn on or off the windshield wipers.

There is also a limit switch at either end of travel for the wipers.
'''

from random import choice
import time

class TaskWindshield:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of windshield wipers.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_STOPPED_AT_LEFT  = 1
    
    ## Constant defining State 2
    S2_STOPPED_AT_RIGHT = 2
    
    ## Constant defining State 3
    S3_MOVING_LEFT      = 3
    
    ## Constant defining State 4
    S4_MOVING_RIGHT     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    def __init__(self, interval, Motor, GoButton, LeftLimit, RightLimit):
        '''
        @brief      Creates a TaskWindshield object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the GoButton object
        self.GoButton = GoButton
        
        ## The button object used for the left limit
        self.LeftLimit = LeftLimit
        
        ## The button object used for the right limit
        self.RightLimit = RightLimit
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S3_MOVING_LEFT)
                self.Motor.Forward()
            
            elif(self.state == self.S1_STOPPED_AT_LEFT):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                
                if self.runs*self.interval > 2:
                    self.transitionTo(self.S5_DO_NOTHING)
                
                elif self.GoButton.getButtonState():
                    self.transitionTo(self.S4_MOVING_RIGHT)
                    self.Motor.Reverse()
            
            elif(self.state == self.S2_STOPPED_AT_RIGHT):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.transitionTo(self.S3_MOVING_LEFT)
                self.Motor.Forward()
            
            elif(self.state == self.S3_MOVING_LEFT):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.LeftLimit.getButtonState():
                    self.transitionTo(self.S1_STOPPED_AT_LEFT)
                    self.Motor.Stop()
            
            elif(self.state == self.S4_MOVING_RIGHT):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.RightLimit.getButtonState():
                    self.transitionTo(self.S2_STOPPED_AT_RIGHT)
                    self.Motor.Stop()
                    
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
                
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CCW')
    
    def Reverse(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CW')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')

