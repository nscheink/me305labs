'''
@file Button.py

This file implements a Button class for controlling an elevator

This file was adapted from an example from the lecture

It implements a virtual button that can be pushed by the user and cleared by the
elevator.
'''
from random import choice

class Button:
    '''
    @brief      A push button class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to indicate a user's desired floor. As of right
                now this class is implemented using "pseudo-hardware". That is,
                we are not working with real hardware IO yet, this is all
                pretend.
    '''

    ## Constant representing the Button off state
    OFF = 0

    ## Constant representing the Button on state
    ON = 1
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin

        ## The internal state of the Button, initialized to Button.OFF
        self.state = Button.OFF
        
        print('Button object created attached to pin '+ str(self.pin))

    def push(self):
        '''
        @brief      Pushes the button to set its state to on
        '''
        self.state = Button.ON

    def clear(self):
        '''
        @brief      Clears the button to set its state to off
        '''
        self.state = Button.OFF
    
    def getButtonState(self):
        '''
        @brief      Gets the Button state.
        @details    Gets the saved virtualized state 
        @return     A boolean representing the state of the button.
        '''
        return self.state


