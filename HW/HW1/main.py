'''
@file main.py

This file demonstrates the finite state machine running

It creates two instances of the finite state machine to show how arbitrary
instances could be made and run asynchronously
'''
from Button import *
from MotorDriver import *
from ProximitySensor import *
from TaskElevator import *
from time import sleep        
if __name__ == '__main__':
    elevatorA = TaskElevator(
                    0.1,
                    MotorDriver(),
                    Button('PB0'),
                    Button('PB1'),
                    ProximitySensor('PB2'),
                    ProximitySensor('PB3')
                )
    elevatorB = TaskElevator(
                    0.1,
                    MotorDriver(),
                    Button('PB0'),
                    Button('PB1'),
                    ProximitySensor('PB2'),
                    ProximitySensor('PB3')
                )

    elevatorA.run()
    sleep(0.2)
    elevatorA.SecondButton.push()
    for i in range(10):
        elevatorA.run()
        sleep(0.2)
    for i in range(10):
        elevatorB.run()
        elevatorA.run()
        sleep(0.2)
    elevatorA.FirstButton.push()
    elevatorB.SecondButton.push()
    for i in range(10):
        elevatorA.run()
        elevatorB.run()
        sleep(0.2)
    elevatorB.FirstButton.push()
    elevatorA.SecondButton.push()
    for i in range(10):
        elevatorA.run()
        elevatorB.run()
        sleep(0.2)
    elevatorA.FirstButton.push()
    elevatorA.run()
    elevatorA.SecondButton.push()
    for i in range(10):
        elevatorA.run()
        elevatorB.run()
        sleep(0.2)
    elevatorB.SecondButton.push()
    for i in range(10):
        elevatorA.run()
        elevatorB.run()
        sleep(0.2)
 
