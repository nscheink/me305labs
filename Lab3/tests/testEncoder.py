import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

import math
def create_pos_delta(c2,c1):
    # c2 and c1 are 16 bit integers
    # diff is arbitrary size
    diff = 0
    if(c2 > 0xC000 and c1 <= 0x4000):
        # Underflow
        diff = c2 - (0xFFFF + c1 + 1)
    elif(c1 > 0xC000 and c2 <= 0x4000):
        # Overflow
        diff = (0xFFFF + c2 + 1) - c1
    else:
        diff = c2 - c1
    return diff


class TestEncoder(unittest.TestCase):

    def test_pos_underflow(self):
        delta = 4
        c1 = 0x0000
        c2 = 0xFFFF - delta + 1
        self.assertEqual(create_pos_delta(c2, c1), -delta)
        
    def test_pos_overflow(self):
        delta = 4
        c1 = 0xFFFF
        c2 = delta - 1
        self.assertEqual(create_pos_delta(c2, c1), delta)

    def test_high_edge_pos(self):
        delta = 4
        c1 = 0xC000 - delta
        c2 = 0xC000 + delta
        self.assertEqual(create_pos_delta(c2, c1), 2*delta)

    def test_high_edge_neg(self):
        delta = 4
        c1 = 0xC000 + delta
        c2 = 0xC000 - delta
        self.assertEqual(create_pos_delta(c2, c1), -2*delta)

    def test_low_edge_pos(self):
        delta = 4
        c1 = 0x4000 - delta
        c2 = 0x4000 + delta
        self.assertEqual(create_pos_delta(c2, c1), 2*delta)

    def test_low_edge_neg(self):
        delta = 4
        c1 = 0x4000 + delta
        c2 = 0x4000 - delta
        self.assertEqual(create_pos_delta(c2, c1), -2*delta)




if __name__ == '__main__':
    unittest.main()
