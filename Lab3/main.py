'''
@file main.py

Loads the encoder and user interface and updates them constantly
'''
import pyb
import time
from Encoder import Encoder
from UserInterface import UserInterface

def main():
    enc1 = Encoder(3, pyb.Pin.board.PA6, 1, pyb.Pin.board.PA7, 2)
    ui1 = UserInterface(enc1)
    while True:
        enc1.update()
        ui1.update()

if __name__ == '__main__':
    main()
