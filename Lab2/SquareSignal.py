'''
@file SquareSignal.py

Implements a signal representing a square wave
'''

from FunctionSignalInterface import FunctionSignalInterface

## Signal class representing a Square wave
class SquareSignal(FunctionSignalInterface):

    ## Returns the square signal at time t
    # @param t float representing the time the signal is currently at.
    # @return A float between 0 and 1 representing the signal value.
    # The signal is normalized to have a period and frequency of 1.
    def valueAt(t: float) -> float:
        return 1 if (t % 1 >= 0.5) else 0
