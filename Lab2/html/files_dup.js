var files_dup =
[
    [ "FunctionSignalInterface.py", "FunctionSignalInterface_8py.html", [
      [ "FunctionSignalInterface", "classFunctionSignalInterface_1_1FunctionSignalInterface.html", "classFunctionSignalInterface_1_1FunctionSignalInterface" ]
    ] ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "run_tests.py", "run__tests_8py.html", "run__tests_8py" ],
    [ "SineSignal.py", "SineSignal_8py.html", [
      [ "SineSignal", "classSineSignal_1_1SineSignal.html", "classSineSignal_1_1SineSignal" ]
    ] ],
    [ "SquareSignal.py", "SquareSignal_8py.html", [
      [ "SquareSignal", "classSquareSignal_1_1SquareSignal.html", "classSquareSignal_1_1SquareSignal" ]
    ] ],
    [ "TaskBlinker.py", "TaskBlinker_8py.html", [
      [ "TaskBlinker", "classTaskBlinker_1_1TaskBlinker.html", "classTaskBlinker_1_1TaskBlinker" ]
    ] ],
    [ "TriangleSignal.py", "TriangleSignal_8py.html", [
      [ "TriangleSignal", "classTriangleSignal_1_1TriangleSignal.html", "classTriangleSignal_1_1TriangleSignal" ]
    ] ]
];