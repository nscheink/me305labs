'''
@file FunctionSignalInterface.py

Implements a Function Signal interface for the LED to call
'''

## Interface for creating signals
class FunctionSignalInterface():

    ## Returns the signal at time t
    # @param t float representing the time the signal is currently at.
    # @return A float between 0 and 1 representing the signal value.
    # The signal is normalized to have a period and frequency of 1.
    def valueAt(self, t: float) -> float:
        pass
