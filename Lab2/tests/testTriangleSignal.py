import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

from TriangleSignal import TriangleSignal
import math

class TestTriangleSignal(unittest.TestCase):
    def test_0(self):
        self.assertAlmostEqual(TriangleSignal.valueAt(0.000001), 0.000001)

    def test_quarter(self):
        self.assertAlmostEqual(TriangleSignal.valueAt(0.25), 0.25)

    def test_half(self):
        self.assertAlmostEqual(TriangleSignal.valueAt(0.5), 0.5)

    def test_three_quarter(self):
        self.assertAlmostEqual(TriangleSignal.valueAt(0.75), 0.75)

    def test_1(self):
        self.assertAlmostEqual(TriangleSignal.valueAt(0.999999), 0.999999)

    def test_1_point_2(self):
        self.assertAlmostEqual(TriangleSignal.valueAt(1.2), 0.2)




if __name__ == '__main__':
    unittest.main()
