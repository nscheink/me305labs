import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

from SquareSignal import SquareSignal
import math

class TestSquareSignal(unittest.TestCase):
    def test_0(self):
        self.assertAlmostEqual(SquareSignal.valueAt(0.000001), 0)

    def test_quarter(self):
        self.assertAlmostEqual(SquareSignal.valueAt(0.25), 0)

    def test_half_below(self):
        self.assertAlmostEqual(SquareSignal.valueAt(0.4999), 0)

    def test_half_above(self):
        self.assertAlmostEqual(SquareSignal.valueAt(0.50001), 1)

    def test_1_below(self):
        self.assertAlmostEqual(SquareSignal.valueAt(0.999999), 1)

    def test_1_above(self):
        self.assertAlmostEqual(SquareSignal.valueAt(1.00001), 0)




if __name__ == '__main__':
    unittest.main()
