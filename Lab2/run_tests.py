'''
@file run_tests.py

This file runs all the unit tests for the created classes

Needs to be run from the source directory to work
'''
import unittest

if __name__ == '__main__':
    testsuite = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=1).run(testsuite)
