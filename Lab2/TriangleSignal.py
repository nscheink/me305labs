'''
@file TriangleSignal.py

Implements a signal representing a triangle wave
'''

from FunctionSignalInterface import FunctionSignalInterface

## Signal class representing a Triangle wave
class TriangleSignal(FunctionSignalInterface):

    ## Returns the triangle signal at time t
    # @param t float representing the time the signal is currently at.
    # @return A float between 0 and 1 representing the signal value.
    # The signal is normalized to have a period and frequency of 1.
    def valueAt(t: float) -> float:
        return t % 1.
