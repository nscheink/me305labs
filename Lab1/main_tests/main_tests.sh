#!/bin/sh 

# Switch to source directory
cd `dirname "$0"`

python ../main.py < first2000.in | grep -Eo '[0-9]+$' > first2000.out
diff first2000.out first2000.expect
